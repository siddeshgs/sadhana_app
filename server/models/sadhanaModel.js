const mongoose = require('mongoose');

const schema = mongoose.Schema;

const sadhanaSchema = new schema({
    name: {
        type: String,
        required: true,
    },
    cycles:{
        type: Number,
        required: false
    },
},{ timestamps: true })

module.exports = mongoose.model('sadhanaModel', sadhanaSchema)