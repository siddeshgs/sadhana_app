const sadhanaSchema = require('../models/sadhanaModel')

const addSadhana = async (req, res ) => {
    try {
        const { name, cycles } = req.body;
        const sadhana = await sadhanaSchema.create({ name, cycles})
        res.status(200).json(sadhana)
    } catch (error) {
        res.status(400).json({ 
            error: error.message
        })
    }
}

const getAllSadhana = async( req, res ) => {
    try{
        const allSadhana = await sadhanaSchema.find({}).sort({ createdAt: -1})
        res.status(200).json(allSadhana)
    }catch(error){
        res.status(400).json({ 
            error: error.message
        })
    }
}

const deletedSadhana = async( req, res ) => {
    try{
        const { id } = req.params;
        const sadhana = await sadhanaSchema.findByIdAndDelete(id)
        res.status(200).json(sadhana)
    }catch(error){
        res.status(400).json({ 
            error: error.message
        })
    }
}

const editSadhana = async( req, res ) => {
    try{
        const { id } = req.params;
        const { name, cycles } = req.body;
        const sadhana = await sadhanaSchema.findByIdAndUpdate({_id: id}, { name, cycles})
        res.status(200).json(sadhana)
    }catch(error){
        res.status(400).json({ 
            error: error.message
        })
    }
}

module.exports = {
    addSadhana,
    getAllSadhana,
    deletedSadhana,
    editSadhana
}