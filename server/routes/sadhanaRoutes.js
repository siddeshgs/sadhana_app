const express = require('express')
const { 
    addSadhana,
    getAllSadhana,
    deletedSadhana,
    editSadhana,
 } = require('../controller/sadhanaController')

const router = express.Router()

router.post('/sadhana/add', addSadhana)
router.get('/sadhana', getAllSadhana)
router.delete('/sadhana/:id', deletedSadhana)
router.put('/sadhana/:id', editSadhana)

module.exports = router