require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const sadhanaRoutes = require('./routes/sadhanaRoutes');
const bodyParser = require('body-parser');

const app = express();

const dotenv = process.env
const PORT = dotenv.PORT
const MONGO_URI = dotenv.MONGO_URI

app.use(bodyParser.json())

app.use((req, res, next)=>{
    console.log(' the request details are: ',req.path, req.method, req.params, req.body)
    next()
})

app.use('/api', sadhanaRoutes)

mongoose.connect(MONGO_URI)
        .then(()=>{            
            app.listen(PORT, ()=>{
                console.log('Server running in port: '+PORT)
            })
        })
        .catch((err)=>{
            console.log(err)
        })
